import static java.lang.Math.*;

public class SearchPerfectNumber {

        public static int work(int num){

            int sum = 1;
            short multi = 2;
            int tmpMulti = 2;
            short step = 0;
            while (num > 1){
                if (num%multi == 0){
                    if (multi == tmpMulti) {
                        step++;
                        sum += pow(multi, step);
                    } else {
                        for (int i = 0; i < step; i++) {
                            sum += multi * pow(tmpMulti, i);
                        }
                        tmpMulti = multi;
                        step = 1;
                    }
                    num /= multi;
                }else  multi += multi%2==0? 1:2;
            }
            return sum;
        }

        public static void main(String[] args) {

            long start = System.currentTimeMillis();
            for (int i = 2; i < 10000000; i++) {
                if (i == work(i)) System.out.println("The perfect number: " + i);
                else System.out.println(i);
            }
            long finish = System.currentTimeMillis();
            System.out.println("Time of work program: " + (finish-start));
        }
}
